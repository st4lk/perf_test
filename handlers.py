import json
from random import uniform, randint
from string import ascii_uppercase
from bson.son import SON

import pymongo
import tornado.web
from tornado.web import HTTPError
from tornado import gen


class BaseHandler(tornado.web.RequestHandler):

    def generate_user_data(self, request_count):
        user_data = {"user": "u%s" % request_count}
        profile_len = ((request_count - 1) % len(ascii_uppercase)) + 1
        profile = {}
        for i in range(profile_len):
            attr_name = "attr_%s" % ascii_uppercase[i]
            attr_value = "%s%s" % (ascii_uppercase[i], randint(0, 200))
            profile[attr_name] = attr_value
        user_data['profile'] = profile
        return user_data

    def get_campaign_query(self, user_data):
        profile = user_data.get('profile', {})
        profile = sorted([(k, v) for k, v in profile.items()])
        attr_names = [a[0] for a in profile]
        params = [
            {"$or": [
                {"target_list": {"$elemMatch": {"target": attr_name, "attr_list": attr_value}}},
                {"target_list.target": {"$nin": [attr_name]}}
            ]} for attr_name, attr_value in profile
        ]
        if not attr_names or not params:
            return None
        return SON([
            ("target_list", {"$not": {"$elemMatch": {"target": {"$nin": attr_names}}}}),
            ("$and", params),
        ])

    def get_request_count_coro(self, db):
        return db.stats.find_and_modify(
            {"name": "req_count"},
            {'$inc': {'value': 1}},
            upsert=True,
            new=True,
            fields={'value': True, '_id': False},
        )

    def render_json(self, data):
        self.set_header('Content-Type', 'application/json')
        self.write(json.dumps(data, indent=4))
        self.finish()

class HomeHandler(tornado.web.RequestHandler):

    def get(self):
        self.write("Performance test project")


class CampaignCreateHandler(BaseHandler):

    def get(self):
        try:
            x = int(self.get_argument('x'))
            y = int(self.get_argument('y'))
            z = int(self.get_argument('z'))
        except ValueError:
            raise HTTPError(400, "bad params")
        campaign_data = self.generate_campaign(x, y, z)
        self.set_header('Content-Disposition', 'attachment; filename=campaign.json')
        self.render_json(campaign_data)

    def generate_campaign(self, x_attrs, y_targets, z_campaigns):
        data = []
        for n, z in enumerate(xrange(z_campaigns), start=1):
            campaign_data = {
                'campaign_name': 'campaign%s' % n,
                'price': float('%.2f' % uniform(1.0, 5000.0)),
            }
            if y_targets:
                targets = []
                for y in xrange(randint(1, y_targets)):
                    letter = ascii_uppercase[y]
                    attr = 'attr_%s' % letter
                    attr_list = []
                    targets.append({'target': attr, 'attr_list': attr_list})
                    if x_attrs:
                        for x in xrange(randint(1, x_attrs)):
                            attr_list.append('%s%s' % (letter, x))
                campaign_data['target_list'] = targets
            data.append(campaign_data)
        return data


class UserCreateHandler(BaseHandler):

    @gen.coroutine
    def get(self):
        db = self.settings['db']
        result = yield self.get_request_count_coro(db)
        request_count = result['value']
        user_data = self.generate_user_data(request_count)
        self.render_json(user_data)


class ImportCampaignHandler(BaseHandler):
    def get(self):
        self.render('templates/upload.html')

    @gen.coroutine
    def post(self):
        campaign_data = self.request.files.get('campaign_data', None)
        if not campaign_data:
            raise HTTPError(400, 'File not provided')
        try:
            campaign_data = json.loads(campaign_data[0]['body'])
        except ValueError:
            raise HTTPError(400, 'Data is not JSON')
        db = self.settings['db']
        result = yield db.campaign.insert(campaign_data)
        self.write("Inserted %s documents" % len(result))


class SearchHandler(BaseHandler):
    def get(self):
        self.render('templates/search.html')

    @gen.coroutine
    def post(self):
        db = self.settings['db']
        try:
            user_data = json.loads(getattr(self.request, 'body', None))
        except (TypeError, ValueError):
            raise HTTPError(400, 'Bad data')
        query = self.get_campaign_query(user_data)
        if not query:
            raise HTTPError(400, 'Bad data')
        campaign = yield db.campaign.find_one(query,
            sort=[('price', pymongo.DESCENDING)],
            fields={"_id": False, "campaign_name": True})
        self.render_json({
            "winner": campaign['campaign_name'] if campaign else "none",
        })


class SearchAutoHandler(BaseHandler):

    @gen.coroutine
    def get(self):
        db = self.settings['db']
        request_count_coro = self.get_request_count_coro(db)
        result = yield request_count_coro
        request_count = result['value']

        user_data = self.generate_user_data(request_count)

        query = self.get_campaign_query(user_data)
        campaign = yield db.campaign.find_one(query,
            sort=[('price', pymongo.DESCENDING)],
            fields={"_id": False, "campaign_name": True})
        self.render_json({
            "winner": campaign['campaign_name'] if campaign else "none",
            "counter": request_count,
        })
