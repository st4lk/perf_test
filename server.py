import motor
import pymongo
import tornado.ioloop
import tornado.web
from tornado.web import url
from tornado import gen

import settings
import handlers

db = motor.motor_tornado.MotorClient(
    **settings.MONGODB['connection'])[settings.MONGODB['db_name']]

application = tornado.web.Application([
    url(r"/", handlers.HomeHandler, name='home'),
    url(r"/campaign", handlers.CampaignCreateHandler, name='campaign'),
    url(r"/user", handlers.UserCreateHandler, name='user'),
    url(r"/import_camp", handlers.ImportCampaignHandler, name='import_camp'),
    url(r"/search", handlers.SearchHandler, name='search'),
    url(r"/search_auto", handlers.SearchAutoHandler, name='search_auto'),
], db=db)


@gen.coroutine
def ensure_index():
    yield db.campaign.ensure_index([('price', pymongo.DESCENDING)])


if __name__ == "__main__":
    application.listen(8888)
    ioloop = tornado.ioloop.IOLoop.instance()
    ioloop.add_callback(ensure_index)
    ioloop.start()
