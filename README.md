Performance test project
========================

Run
---

- start mongod service (tested with v3.0.6)
- make sure python2.7 is installed
- install dependencies

        pip install -r requirements.txt

- run server:

        python server.py --logging=warning

Server will listen localhost:8888

Resources
---------

- /campaign?x=..&y=..&z=..

    Generate campaign data

- /user

    Generate user data

- /import_camp

    Import campaign data

- /search

    Search campaign by user_data

- /search_auto

    Generate user data and search campaign

Performance
-----------

Runned on

- 2,2 GHz Intel Core i7
- 16 Gb RAM
- /campaign?x=50&y=10&z=1000

        $ ./wrk -c 64 -d 10s http://127.0.0.1:8888/search_auto
        Running 10s test @ http://127.0.0.1:8888/search_auto
          2 threads and 64 connections
          Thread Stats   Avg      Stdev     Max   +/- Stdev
            Latency   187.72ms   24.20ms 342.36ms   86.25%
            Req/Sec   170.57     50.23   297.00     72.08%
          3396 requests in 10.09s, 775.06KB read
        Requests/sec:    336.61
        Transfer/sec:     76.82KB
