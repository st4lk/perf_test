import tornado
from tornado.options import define

define("port", default=8888, help="run on the given port", type=int)
define("config", default=None, help="tornado config file")
define("debug", default=False, help="debug mode")

tornado.options.parse_command_line()

MONGODB = {
    'connection': {
        'host': '127.0.0.1',
        'port': 27017,
        'max_pool_size': 100,
    },
    'db_name': 'perf_test',
}
